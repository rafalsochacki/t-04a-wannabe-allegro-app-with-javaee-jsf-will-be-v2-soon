package pl.codementors.finalproject.model;

import org.junit.Assert;
import org.junit.Test;

public class UserTest {

    @Test
    public void UserWhenDataProvided() {
        String name = "Krzesimir";
        String surname = "Jakiśtam";
        String email = "jakisemail@email.pl";
        String login = "krzesimir";
        String password = "password";
        String role = "ADMIN";
        Boolean authorization = false;

        User user = new User(name, surname, email, login, password, role, authorization);

        Assert.assertEquals("name not set", name, user.getName());
        Assert.assertEquals("surname not set", surname, user.getSurname());
        Assert.assertEquals("email not set", email, user.getEmail());
        Assert.assertEquals("login not set", login, user.getLogin());
        Assert.assertEquals("password not set", password, user.getPassword());
        Assert.assertEquals("role not set", role, user.getRole());
        Assert.assertEquals("authorization not set", authorization, user.isAuthorization());

    }

    @Test
    public void UserWhereDataIsNotProvided(){
        User user = new User();

        Assert.assertNull(user.getName());
        Assert.assertNull(user.getSurname());
        Assert.assertNull(user.getEmail());
        Assert.assertNull(user.getLogin());
        Assert.assertNull(user.getPassword());
        Assert.assertNull(user.getRole());
        Assert.assertFalse(user.isAuthorization());
    }

    @Test
    public void setName() {
        User user = new User();
        user.setName("Krzesimir");
        Assert.assertEquals("name not set properly", "Krzesimir", user.getName());
    }

    @Test
    public void setSurname() {
        User user = new User();
        user.setSurname("Jakiśtam");
        Assert.assertEquals("surname not set properly", "Jakiśtam", user.getSurname());
    }

    @Test
    public void setEmail() {
        User user = new User();
        user.setEmail("jakisemail@email.pl");
        Assert.assertEquals("email not set properly", "jakisemail@email.pl", user.getEmail());
    }

    @Test
    public void setLogin() {
        User user = new User();
        user.setLogin("krzesimir");
        Assert.assertEquals("login not set properly", "krzesimir", user.getLogin());
    }

    @Test
    public void setPassword() {
        User user = new User();
        user.setPassword("password");
        Assert.assertEquals("password not set properly", "password", user.getPassword());
    }

    @Test
    public void setRole() {
        User user = new User();
        user.setRole("USER");
        Assert.assertEquals("role not set properly", "USER", user.getRole());
    }

    @Test
    public void setAuthorization() {
        User user = new User();
        user.setAuthorization(false);
        Assert.assertFalse("authorization not set properly", user.isAuthorization());
    }

}
