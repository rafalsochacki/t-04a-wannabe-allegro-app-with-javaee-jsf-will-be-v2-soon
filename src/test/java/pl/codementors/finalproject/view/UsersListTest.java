package pl.codementors.finalproject.view;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.runners.MockitoJUnitRunner;
import pl.codementors.finalproject.UserDataStore;
import pl.codementors.finalproject.model.User;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UsersListTest {

    private static List<User> USERS;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        USERS = new ArrayList<User>() {{
            add(new User("John", "Doe"));
        }};
    }
    @Mock
    private UserDataStore userDataStore;

    @InjectMocks
    private UsersList usersList;

    @InjectMocks
    private static final User user = new User("Jane", "Doe");


    @Test
    public void should_fetch_users_list_from_data_store() {
        when(userDataStore.getUsers()).thenReturn(USERS);

        List<User> users = usersList.getUsers();

        Assert.assertEquals(USERS, users);
    }

    @Test
    public void should_fetch_users_list_from_data_store_only_once() {
        when(userDataStore.getUsers()).thenReturn(USERS);
        usersList.getUsers();

        List<User> users = usersList.getUsers();

        Assert.assertEquals(USERS, users);
        Mockito.verify(userDataStore, VerificationModeFactory.times(1)).getUsers();
    }


//    @Test
//    public void delete_should_remove_user_form_user_data_store() {
//        when(userDataStore.getUsers()).thenReturn(USERS);
//
//        List <User> users = usersList.getUsers();
//        doNothing().when(usersList).delete(any(User.class));
//
//        assertEquals(USERS,users);
//        verify(userDataStore, times(1)).getUsers();
//    }
//


}