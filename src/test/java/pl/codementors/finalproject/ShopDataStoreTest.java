package pl.codementors.finalproject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.codementors.finalproject.model.Product;
import pl.codementors.finalproject.model.User;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ShopDataStoreTest {

    @Mock
    private EntityManager entityManager;
    @InjectMocks
    private ShopDataStore shopDataStore;

    @Test
    public void should_find_user_with_given_id() {
        User userTest = new User("Ktos", "Jakis tam", "jakisemail", "jakislogin", "jakieshaslo", "USER", false);

        when(entityManager.find(User.class, 1)).thenReturn(userTest);

        final User user = shopDataStore.getUser(1);
        assertEquals(user, userTest);
    }

    @Test
    public void should_find_product_with_given_id() {
        Product testProduct = new Product("testproduct", new User(), 120, true);

        when(entityManager.find(Product.class, 1)).thenReturn(testProduct);

        Product product = shopDataStore.getProduct(1);
        assertEquals(product, testProduct);
    }

//    @Test
//    public void should_remove_product_with_given_id() {
//        Product testProduct = new Product("testproduct", new User(), 120, true);
//
//        when(entityManager.find(Product.class,1)).thenReturn(testProduct);
//        when(entityManager.remove(testProduct)).thenReturn(1);
//
//        shopDataStore.removeProduct(1);
//
//        assertArrayEquals("should be equals", );
//    }
}