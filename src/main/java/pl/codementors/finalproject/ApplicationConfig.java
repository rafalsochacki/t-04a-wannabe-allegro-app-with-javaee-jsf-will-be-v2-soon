package pl.codementors.finalproject;

import pl.codementors.finalproject.resource.ProductResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Application Configuration for our REST methods.
 */
@ApplicationPath("/api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<>(Arrays.asList(ProductResource.class, CorsFilter.class));
    }
}
