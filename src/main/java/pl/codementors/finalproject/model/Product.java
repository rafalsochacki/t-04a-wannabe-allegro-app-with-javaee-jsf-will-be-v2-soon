package pl.codementors.finalproject.model;

import javax.persistence.*;

/**
 * Single product representation / entity
 */
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    /**
     * User who sells stuff;
     */
    @ManyToOne
    private User seller;

    @Column
    private int price;

    @Column
    private boolean available;

    public Product() {

    }

    public Product(User seller) {
        this.seller = seller;
        this.available = true;
    }

    public Product(String name, User seller, int price, boolean available) {
        this.name = name;
        this.seller = seller;
        this.price = price;
        this.available = available;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
