package pl.codementors.finalproject.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Single order representation/entity which contains buyer data and list of products.
 */
@Entity
@Table(name = "`order`")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * relationship - one order to many products.
     * to get products on the view we will use EAGER - instant add data to the field.
     */
    @OneToMany(fetch = FetchType.EAGER)
    private List<Product> products = new ArrayList<>();

    /**
     * relationship - many orders to one user.
     */
    @ManyToOne
    private User user;

    public Order(List<Product> products, User user) {
        this.products = products;
        this.user = user;
    }

    public Order() {}

    public List<Product> getProducts() {
        return products;
    }
}
