package pl.codementors.finalproject.view;

import pl.codementors.finalproject.ShopDataStore;
import pl.codementors.finalproject.model.Product;
import pl.codementors.finalproject.model.User;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * class to edit a product.
 */
@Named
@ViewScoped
public class ProductEdit implements Serializable {

    /**
     * ShopDataStore as EJB container.
     */
    @EJB
    private ShopDataStore store;

    private int productId;

    private boolean editMode;

    private Product product;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * currently logged user.
     */
    private User user;

    // Na razie na sztywno zakladamy, ze jestesmy zalogowani jako user o id 1.
    @PostConstruct
    public void onInit() {
        user = store.getUser(1);
    }

    /**
     * method to edit selected product.
     * @return changed or new product.
     */
    public Product getProduct() {
        if (product == null) {
            if(editMode) {
                product = store.getProduct(productId);
            } else {
                product = new Product(user);
            }
        }
        return product;
    }

    /**
     * method to save edited product.
     */
    public void saveProduct() {
        if(editMode) {
            store.updateProduct(product);
        } else {
            store.createProduct(product);
        }
    }
}

