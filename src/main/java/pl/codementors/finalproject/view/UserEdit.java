package pl.codementors.finalproject.view;

import pl.codementors.finalproject.UserDataStore;
import pl.codementors.finalproject.model.User;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class UserEdit implements Serializable {

    @EJB
    private UserDataStore userDataStore;

    private int userId;

    private User user;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        if (user == null) {
            user = userDataStore.getUser(userId);
        }
        return user;
    }

    public void saveUser() {
        userDataStore.updateUser(user);
    }





}
