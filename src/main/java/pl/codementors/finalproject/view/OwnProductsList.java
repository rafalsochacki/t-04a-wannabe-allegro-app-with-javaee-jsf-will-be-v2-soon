package pl.codementors.finalproject.view;

import pl.codementors.finalproject.ShopDataStore;
import pl.codementors.finalproject.bean.ShoppingCart;
import pl.codementors.finalproject.model.Product;
import pl.codementors.finalproject.model.User;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class OwnProductsList implements Serializable {

    /**
     * shopping cart as a bean EJB container.
     */
    @EJB
    private ShoppingCart shoppingCart;

    /**
     * ShopDataStore as EJB container.
     */
    @EJB
    private ShopDataStore store;

    /**
     * List of products.
     */
    private List<Product> products;

    /**
     * currently logged user.
     */
    private User user;

    public OwnProductsList() {
    }

    // Na razie na sztywno zakladamy, ze jestesmy zalogowani jako user o id 1.
    @PostConstruct
    public void onInit() {
        user = store.getUser(1);
    }

    /**
     * @return list of products currently logged user.
     */
    public List<Product> getProducts() {
        if (products == null) {
            products = store.getOwnProducts(user);
        }
        return products;
    }

    public User getUser() {
        return user;
    }

    /**
     * adds product to the shopping cart.
     */
    public void addToShoppingCart(int id) {
        shoppingCart.addId(id);
    }

    /**
     * remove product from shopping cart.
     */
    public void removeProduct(int id) {
        store.removeProduct(id);
        products.removeIf(product -> product.getId() == id);
    }

}
