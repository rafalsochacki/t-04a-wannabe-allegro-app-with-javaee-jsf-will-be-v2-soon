package pl.codementors.finalproject.view;

import pl.codementors.finalproject.UserDataStore;
import pl.codementors.finalproject.model.User;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@RequestScoped
public class UsersList implements Serializable {

    @EJB
    private UserDataStore userDataStore;


    private List<User> users;

    public List<User> getUsers() {
        if (users == null) {
            users = userDataStore.getUsers();
        }
        return users;
    }

    public void delete(User user) {
        userDataStore.deleteUser(user);
        users = null;
    }

    public User add(User user) {
        userDataStore.createUser(user);
        return user;
    }

}
