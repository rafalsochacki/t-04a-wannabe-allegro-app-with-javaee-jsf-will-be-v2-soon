package pl.codementors.finalproject.view;

import pl.codementors.finalproject.ShopDataStore;
import pl.codementors.finalproject.bean.ShoppingCart;
import pl.codementors.finalproject.model.Order;
import pl.codementors.finalproject.model.User;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * List of orders.
 */
@Named
@ViewScoped
public class OrdersList implements Serializable {

    /**
     * shopping cart as a bean EJB container.
     */
    @EJB
    private ShoppingCart shoppingCart;

    /**
     * ShopDataStore as EJB container.
     */
    @EJB
    private ShopDataStore store;

    private List<Order> orders;

    /**
     * currently logged user.
     */
    private User user;

    public OrdersList() {
    }

    //na sztywno jako user o id 1,
    @PostConstruct
    public void onInit() {
        user = store.getUser(1);
    }

    /**
     *
     * @return list of orders.
     */
    public List<Order> getOrders() {
        if (orders == null) {
            orders = store.getOrdersForUser(user);
        }
        return orders;
    }

    public User getUser() {
        return user;
    }
}

