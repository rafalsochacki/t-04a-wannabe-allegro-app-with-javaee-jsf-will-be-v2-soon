package pl.codementors.finalproject.view;

import pl.codementors.finalproject.ShopDataStore;
import pl.codementors.finalproject.bean.ShoppingCart;
import pl.codementors.finalproject.model.Product;
import pl.codementors.finalproject.model.User;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class ProductsList implements Serializable {

    /**
     * shopping cart as a bean EJB container.
     */
    @EJB
    private ShoppingCart shoppingCart;

    /**
     * ShopDataStore as EJB container.
     */
    @EJB
    private ShopDataStore store;

    /**
     * List of products.
     */
    private List<Product> products;

    /**
     * current logged user.
     */
    private User user;

    public ProductsList() {

    }

    //tymczasowe sztywne zalozenie ze jestemy userem o id 1;
    @PostConstruct
    public void onInit() {
        user = store.getUser(1);
    }

    /**
     * @return list of products.
     */
    public List<Product> getProducts() {
        if(products == null){
            products = store.getAvailableProducts();
        }
        return products;
    }

    public User getUser() {
        return user;
    }

    /**
     * @param id of product to put into the shopping cart.
     */
    public void addToShoppingCart(int id) {
        shoppingCart.addId(id);
    }
}
