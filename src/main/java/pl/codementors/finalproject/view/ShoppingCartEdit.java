package pl.codementors.finalproject.view;

import pl.codementors.finalproject.ShopDataStore;
import pl.codementors.finalproject.bean.ShoppingCart;
import pl.codementors.finalproject.model.Order;
import pl.codementors.finalproject.model.Product;
import pl.codementors.finalproject.model.User;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to edit shopping cart.
 */
@Named
@ViewScoped
public class ShoppingCartEdit implements Serializable {

    /**
     * shopping cart as a bean EJB container.
     */
    @EJB
    private ShoppingCart shoppingCart;

    /**
     * ShopDataStore as EJB container.
     */
    @EJB
    private ShopDataStore store;

    private String name;
    private String surname;

    private List<Product> products;

    /**
     * current logged user.
     */
    private User user;

    // Na razie na sztywno zakladamy, ze jestesmy zalogowani jako user o id 1.
    @PostConstruct
    public void onInit() {
        user = store.getUser(1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Product> getProducts() {
        // if we havent yet assigned the value of the products field
        if (products == null) {
            //we will get products with id same as products from shopping cart
            products = shoppingCart.getProductsId()
                    .stream()
                    .map(id -> store.getProduct(id))
                    .collect(Collectors.toList());
        }
        return products;
    }

    /**
     * Save the shopping cart
     */
    public void save() {
        //if we have in shopping cart product which is not available.
        if(products.stream().anyMatch(product -> !product.isAvailable())) {
            //all unavailable products will be deleted.
            products.removeIf(product -> !product.isAvailable());
            return;
        }
        //for our product
        products.forEach(product -> {
            // we will set unavailable status
            product.setAvailable(false);
            // and updates the product
            store.updateProduct(product);
        });
        // we are creating a new order includes products from above.
        Order order = new Order(products, user);
        store.createOrder(order);
        // clean up the shopping cart
        shoppingCart.reset();
        // and list of products.
        products.clear();
    }

    public void removeFromCart(int id) {
        shoppingCart.removeId(id);
        products.removeIf(antique -> antique.getId() == id);
    }
}

