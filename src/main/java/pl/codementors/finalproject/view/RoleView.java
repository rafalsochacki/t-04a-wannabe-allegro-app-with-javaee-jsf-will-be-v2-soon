package pl.codementors.finalproject.view;

import pl.codementors.finalproject.UserDataStore;
import pl.codementors.finalproject.model.User;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.security.Principal;


@ViewScoped
@Named
public class RoleView implements Serializable {

    @EJB
    private UserDataStore userDataStore;

    @Inject
    private Principal principal;

    public boolean isAdmin() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("ADMIN");

    }
    public boolean isUser() {
        return FacesContext.getCurrentInstance().getExternalContext().isUserInRole("USER");
    }

    public User getCurrentUser() {
        return userDataStore.getUserByName(principal.getName())
                .orElse(new User("John", "Doe"));

    }

}
