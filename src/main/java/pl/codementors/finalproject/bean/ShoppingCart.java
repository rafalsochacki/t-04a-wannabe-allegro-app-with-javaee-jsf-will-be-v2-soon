package pl.codementors.finalproject.bean;

import javax.ejb.Singleton;
import java.util.HashSet;
import java.util.Set;

/**
 * Single shopping cart representation.
 * Singleton - there can be only 1 instance of shopping cart per user/session.
 */
@Singleton
public class ShoppingCart {

    /**
     * Ids of products.
     */
    private Set<Integer> productsId = new HashSet<>();

    /**
     * @return id of product.
     */
    public Set<Integer> getProductsId() {
        return productsId;
    }

    /**
     * add id to a product.
     * @param id
     */
    public void addId(int id) {
        productsId.add(id);
    }

    /**
     * remove id of product.
     * @param id
     */
    public void removeId(int id) {
        productsId.remove(id);
    }

    public void reset() {
        productsId.clear();
    }
}
