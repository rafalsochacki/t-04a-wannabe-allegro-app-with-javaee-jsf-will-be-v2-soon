package pl.codementors.finalproject;

import org.apache.commons.codec.digest.DigestUtils;
import pl.codementors.finalproject.model.Product;
import pl.codementors.finalproject.model.User;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.logging.Logger;
/**
 * Class to keep some hardcoded data about products and users.
 */
@Startup
@Singleton
public class ShopDataStoreContext {

    private static final Logger log = Logger.getLogger(ShopDataStoreContext.class.getName());

    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    public void init() {
//        przed passwordem DigestUtils.sha256Hex
        User admin = new User("name", "surname", "admin@gmail.com","admin",  "admin", "ADMIN",true);
        User user = new User("name", "surname", "user@gmail.com", "user", "user", "USER",true);
        entityManager.persist(admin);
        entityManager.persist(user);

        Product product1 = new Product("Produkt1", user, 123, true);
        entityManager.persist(product1);
        Product product2 = new Product("Produkt2", user, 321, false);
        entityManager.persist(product2);
        Product product3 = new Product("Produkt3", user, 555, true);
        entityManager.persist(product3);
        Product product4 = new Product("Produkt4", user, 876, true);
        entityManager.persist(product4);
        Product product5 = new Product("Produkt5", admin, 1231, true);
        entityManager.persist(product5);

    }
}
