package pl.codementors.finalproject;

import pl.codementors.finalproject.model.User;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * DAO class to communicate with dataBase.
 */
@Singleton
public class UserDataStore {

    private static final Logger log = Logger.getLogger(UserDataStore.class.getName());

    @PersistenceContext
    private EntityManager em;

    /**
     * @return list of all users.
     */
    public List<User> getUsers() {
        TypedQuery<User> query = em.createQuery("select e from User e", User.class);
        return query.getResultList();
    }


    public Optional<User> getUserByName(String name) {
        try {
            return Optional.of(em.createQuery("select u from User u where u.name=:name", User.class)
                    .setParameter("name", name)
                    .getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    /**
     * @param id to get specific user form database.
     * @return user.
     */
    //??? 50/50 czy dobrze to podziala
    public User getUser(int id) {
        User user = em.find(User.class, id);
        em.detach(user);
        user.setId(user.getId());
        return user;
    }

    /**
     * @param user to create a new user in database.
     */
    public void createUser(User user) {
        em.persist(user);
    }

    /**
     * @param user to update specific user in database
     */
    public void updateUser(User user) {
        em.merge(user);
    }

    /**
     * @param user to delete specific user from database.
     */
    public void deleteUser(User user) {
        em.remove((em.merge(user)));
    }

}
