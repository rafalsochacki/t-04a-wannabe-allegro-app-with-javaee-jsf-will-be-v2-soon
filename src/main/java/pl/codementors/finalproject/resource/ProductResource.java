package pl.codementors.finalproject.resource;

import pl.codementors.finalproject.ShopDataStore;
import pl.codementors.finalproject.model.Product;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * REST request to the address /api/products.
 */
@Path("products")
public class ProductResource {
    @Inject
    private ShopDataStore store;

    /**
     * REST request handling.
     * @return a list of all available products.
     */
    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Product> getProducts() {
        return store.getAvailableProducts();
    }

}

