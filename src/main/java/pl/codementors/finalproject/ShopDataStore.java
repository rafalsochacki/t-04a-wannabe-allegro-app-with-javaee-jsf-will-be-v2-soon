package pl.codementors.finalproject;

import pl.codementors.finalproject.model.Order;
import pl.codementors.finalproject.model.Product;
import pl.codementors.finalproject.model.User;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.logging.Logger;

/**
 * DAO class to communicate with dataBase.
 */
@Singleton
public class ShopDataStore {
    private static final Logger log = Logger.getLogger(ShopDataStore.class.getName());

    /**
     * Simple injection of Entity Menager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * @return list of available products.
     */
    public List<Product> getAvailableProducts() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> query = cb.createQuery(Product.class);
        Root<Product> root = query.from(Product.class);
        query.where(cb.equal(root.get("available"), true));
        return entityManager.createQuery(query).getResultList();
    }

    /**
     * @param id to get specific user.
     * @return user
     */
    public User getUser(int id) {
        User user = entityManager.find(User.class, id);
        entityManager.detach(user);
        return user;
    }

    /**
     * @param id to get specific product.
     * @return product.
     */
    public Product getProduct(int id) {
        Product product = entityManager.find(Product.class, id);
        entityManager.detach(product);
        return product;
    }

    /**
     * delete a product.
     * @param id of product to delete.
     */
    public void removeProduct(int id) {
        Product product = entityManager.find(Product.class, id);
        entityManager.remove(product);
    }

    /**
     * @return all products of specific user.
     */
    public List<Order> getOrdersForUser(User user) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> query = cb.createQuery(Order.class);
        Root<Order> root = query.from(Order.class);
        query.where(cb.equal(root.get("user"), user));
        return entityManager.createQuery(query).getResultList();
    }

    /**
     * @return all orders of specific user.
     */
    public List<Product> getOwnProducts(User user) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> query = cb.createQuery(Product.class);
        Root<Product> root = query.from(Product.class);
        query.where(cb.equal(root.get("seller"), user));
        return entityManager.createQuery(query).getResultList();
    }

    /**
     * updates the specific product.
     */
    public void updateProduct(Product product) {
        entityManager.merge(product);
    }

    /**
     * @param order to create a new order.
     */
    public void createOrder(Order order) {
        entityManager.persist(order);
    }

    /**
     * @param product to create a new product.
     */
    public void createProduct(Product product) {
        entityManager.persist(product);
    }

}
